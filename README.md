# spring security oauth2 单点登录（客户端） 

1. 你需要准备oauth2 认证服务器，它是独立的，自己去网上找一个。
2. 配置文件中`security.oauth2`中的内容需要与认证服务器的实际配置一直，`client`信息也是认证服务发放。



效果:

1. 启动本demo,然后打开  http://localhost:8080/demo/ 会重定向到认证服务器的登录页面
2. 修改端口为8081，启动第二个demo，无需登录即可直接进入页面。