package me.jclazz.demo.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User Controller
 *
 * @author CJ (jclazz@outlook.com)
 * @date 2019/10/15
 * @since 1.0
 */
@RestController
public class UserController {
    @GetMapping("/user")
    Authentication userInfo(){
        return SecurityContextHolder.getContext().getAuthentication();
    }
}
